import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.Properties;

/**
 * Enables connection with a database and supports all operations on it
 * 
 * @author Dominika D�bek
 *
 */
public class DatabaseHandler {
	
	private Connection connection = null;
	private Statement statement = null;
	private ResultSet resultSet = null;
	
	public DatabaseHandler(){
		loadDriver();
		connectToDatabase("localhost", 3306);
		createStatement(connection);
		prepareDatabase();
	}
	
	@SuppressWarnings("deprecation")
	public boolean loadDriver() {
	    try {
	        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
	        return true;
	    } catch (Exception e) {
	        System.out.println("Error when loading the driver for the database");
	        return false;
	    }
	}
	
	public Connection connectToDatabase(String adress, int port) {
        Properties connectionProps = new Properties();
        connectionProps.put("user", "root");
        connectionProps.put("password", "root");
        connectionProps.put("useUnicode", "true");
        connectionProps.put("useJDBCCompliantTimezoneShift", "true");
        connectionProps.put("useLegacyDatatimeCode", "false");
        connectionProps.put("serverTimezone", "UTC");
        connectionProps.put("useSSL", "false");
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + adress + ":" + port + "/",
                    connectionProps);
        } catch (SQLException e) {
        	System.out.println("Error when connecting to the database");
            e.printStackTrace();
        }
        return connection;
    }

	public Statement createStatement(Connection connection) {
	    try {
	    	statement = connection.createStatement();
	        return statement;
	    } catch (SQLException e) {
	    	System.out.println("Error when creating statement for database");
	        e.printStackTrace();
	    }
	    return null;
	}
	
	public void prepareDatabase()  {
		ResultSet resultSet = executeQuery(statement, "SHOW DATABASES;");
		boolean flag = true;
		try{
			while(resultSet.next())
				if(resultSet.getString("Database").equals("surveys"))
					flag = false;
		}catch(SQLException sqlException) {
			System.out.println("Database access error or other errors");
			sqlException.printStackTrace();
		}
		if(flag) {
			executeUpdate(statement, "CREATE DATABASE SURVEYS;");
			executeUpdate(statement, "USE SURVEYS");
			executeUpdate(statement, "CREATE TABLE SURVEY(ID INT PRIMARY KEY AUTO_INCREMENT, NAME VARCHAR(255) NOT NULL, "
					+ "QUESTION1 VARCHAR(255) NOT NULL, ANSWER11 VARCHAR(255) NOT NULL, ANSWER12 VARCHAR(255) NOT NULL, ANSWER13 VARCHAR(255) NOT NULL, "
					+ "QUESTION2 VARCHAR(255) NOT NULL, ANSWER21 VARCHAR(255) NOT NULL, ANSWER22 VARCHAR(255) NOT NULL, ANSWER23 VARCHAR(255) NOT NULL, "
					+ "QUESTION3 VARCHAR(255) NOT NULL, ANSWER31 VARCHAR(255) NOT NULL, ANSWER32 VARCHAR(255) NOT NULL, ANSWER33 VARCHAR(255) NOT NULL);");
			executeUpdate(statement,  "CREATE TABLE ANSWER(ID_ANSWER INT PRIMARY KEY AUTO_INCREMENT, ID_SURVEY INT NOT NULL,"
					+ "ID_USER VARCHAR(255), ANSWER1 VARCHAR(255) NOT NULL, ANSWER2 VARCHAR(255) NOT NULL, ANSWER3 VARCHAR(255) NOT NULL);");
			executeUpdate(statement, "CREATE TABLE RESULT(ID_RESULT INT PRIMARY KEY AUTO_INCREMENT, ID_SURVEY INT NOT NULL, ANSWER1A INT NOT NULL, "
					+ "ANSWER1B INT NOT NULL, ANSWER1C INT NOT NULL, ANSWER2A INT NOT NULL, ANSWER2B INT NOT NULL, ANSWER2C INT NOT NULL, ANSWER3A INT NOT NULL,"
					+ "ANSWER3B INT NOT NULL, ANSWER3C INT NOT NULL);");
			executeUpdate(statement, "INSERT INTO RESULT(ID_SURVEY, ANSWER1A, ANSWER1B, ANSWER1C, ANSWER2A, ANSWER2B, ANSWER2C, ANSWER3A, ANSWER3B, ANSWER3C) VALUES(1, 0, 0, 0, 0, 0, 0, 0, 0, 0);");
			executeUpdate(statement, "INSERT INTO RESULT(ID_SURVEY, ANSWER1A, ANSWER1B, ANSWER1C, ANSWER2A, ANSWER2B, ANSWER2C, ANSWER3A, ANSWER3B, ANSWER3C) VALUES(2, 0, 0, 0, 0, 0, 0, 0, 0, 0);");
			executeUpdate(statement, "INSERT INTO RESULT(ID_SURVEY, ANSWER1A, ANSWER1B, ANSWER1C, ANSWER2A, ANSWER2B, ANSWER2C, ANSWER3A, ANSWER3B, ANSWER3C) VALUES(3, 0, 0, 0, 0, 0, 0, 0, 0, 0);");
			executeUpdate(statement, "INSERT INTO RESULT(ID_SURVEY, ANSWER1A, ANSWER1B, ANSWER1C, ANSWER2A, ANSWER2B, ANSWER2C, ANSWER3A, ANSWER3B, ANSWER3C) VALUES(4, 0, 0, 0, 0, 0, 0, 0, 0, 0);");
			executeUpdate(statement, "INSERT INTO SURVEY(NAME, QUESTION1, ANSWER11, ANSWER12, ANSWER13, QUESTION2, ANSWER21, "
					+ "ANSWER22, ANSWER23, QUESTION3, ANSWER31, ANSWER32, ANSWER33) VALUES(\"survey1\", \"In what age range are you?\", "
					+ "\"18-30\", \"30-50\", \"50-70\", \"How much do you earn?\", \"2000-4000$\", \"4000-6000$\", "
					+ "\"6000-8000\", \"How many inhabitants does your city have?\", \"< 10 000\", \"< 100 000\", \"more than\");");
			executeUpdate(statement, "INSERT INTO SURVEY(NAME, QUESTION1, ANSWER11, ANSWER12, ANSWER13, QUESTION2, ANSWER21, "
					+ "ANSWER22, ANSWER23, QUESTION3, ANSWER31, ANSWER32, ANSWER33) VALUES(\"survey2\", \"AAAIn what age range are you?\", "
					+ "\"AAA 18-30\", \"AAA 30-50\", \"AAA 50-70\", \"AAAHow much do you earn?\", \"AAA 2000-4000$\", \"AAA 4000-6000$\", "
					+ "\"AAA 6000-8000\", \"AAAHow many inhabitants does your city have?\", \"AAA < 10 000\", \"AAA < 100 000\", \"AAA more than\");");
			executeUpdate(statement, "INSERT INTO SURVEY(NAME, QUESTION1, ANSWER11, ANSWER12, ANSWER13, QUESTION2, ANSWER21, "
					+ "ANSWER22, ANSWER23, QUESTION3, ANSWER31, ANSWER32, ANSWER33) VALUES(\"survey3\", \"BBBIn what age range are you?\", "
					+ "\"BBB 18-30\", \"BBB 30-50\", \"BBB 50-70\", \"BBBHow much do you earn?\", \"BBB 2000-4000$\", \"BBB 4000-6000$\", "
					+ "\"BBB 6000-8000\", \"BBBHow many inhabitants does your city have?\", \"BBB < 10 000\", \"BBB < 100 000\", \"BBB more than\");");
			executeUpdate(statement, "INSERT INTO SURVEY(NAME, QUESTION1, ANSWER11, ANSWER12, ANSWER13, QUESTION2, ANSWER21, "
					+ "ANSWER22, ANSWER23, QUESTION3, ANSWER31, ANSWER32, ANSWER33) VALUES(\"survey4\", \"CCCIn what age range are you?\", "
					+ "\"CCC 18-30\", \"CCC 30-50\", \"CCC 50-70\", \"CCCHow much do you earn?\", \"CCC 2000-4000$\", \"CCC 4000-6000$\", "
					+ "\"CCC 6000-8000\", \"CCCHow many inhabitants does your city have?\", \"CCC < 10 000\", \"CCC < 100 000\", \"CCC more than\");");
		}else {
			executeUpdate(statement, "USE SURVEYS;");
		}
	}
	
	public ResultSet executeQuery(Statement s, String sql) {
	    try {
	        return s.executeQuery(sql);
	    } catch (SQLException e) {
	    	System.out.println("Error while executing the query on the database");
	        e.printStackTrace();
	    }
	    return null;
	}
	
	public int executeUpdate(Statement s, String sql) {
	       try {
	           return s.executeUpdate(sql);
	       } catch (SQLException e) {
	    	   System.out.println("Error while executing the query without result on the database");
	           e.printStackTrace();
	       }
	       return -1;
	}
	 
	public String getSurveyAmount() {
		Object obj = null;
		resultSet = executeQuery(statement, "SELECT COUNT(*) FROM SURVEY;");
		try {
			resultSet.next();
			obj = resultSet.getObject(1);
		}catch(SQLException sqlException) {
			System.out.println("Error while executing the query without result on the database");
	        sqlException.printStackTrace();
		}
		return obj.toString();
	}
	public String getNameSurveys() throws SQLException {
		resultSet = executeQuery(statement, "SELECT NAME FROM SURVEY;");
		String allNames ="";
		int number = 1;
		 while (resultSet.next()) {
             for (int i = 1; i <= 1; i++) {
                 Object obj = resultSet.getObject(i);
                 if (obj != null) {
                    allNames += number + ") " + obj.toString() + "\n";
                    number++;
                 }
             }
         }
		 return allNames;
	}
	
	public String getQuestionSurvey(String numberSurvey, String numberQuestion, String numberFirstAnswer, String numberSecondAnser, String numberThirdAnswer) throws SQLException {
		resultSet = executeQuery(statement, "SELECT '" + numberQuestion + "', '" + numberFirstAnswer + "', '" + numberSecondAnser + "', '" + numberThirdAnswer + "' FROM SURVEY WHERE ID = " + numberSurvey + ";");
		String allNames ="";
		 while (resultSet.next()) {
             for (int i = 1; i <= 4; i++) {
                 Object obj = resultSet.getObject(i);
                 if (obj != null) {
                    allNames +=  obj.toString() + "\n";
                 }
             }
         }
		 return allNames;
	}
	

	 
	public ResultSet getSurveyById(int currentSurvey) {
		resultSet = executeQuery(statement, "SELECT * FROM SURVEY WHERE ID = " + currentSurvey + ";");
		try {
			resultSet.next();
		}catch(SQLException sqlException) {
			System.out.println("Error while executing the query without result on the database");
	        sqlException.printStackTrace();
		}
		return resultSet;
	}
	
	public boolean checkExistId(String id) {
		resultSet = executeQuery(statement, "SELECT * FROM ANSWER WHERE ID_USER = " + id + ";");
		try {
			resultSet.next();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(readValueOne() == null) {
			return false;
		}
		System.out.println("Id exists, choose other");
		return true;
	}
	
	public String readValueOne() {
		Object obj;
		try {
			obj = resultSet.getObject(1);
		}catch(SQLException sqlException) {
			return null;
		}
	    return obj.toString();
	}
	
	public String readValue(int index) {
		Object obj;
		try {
			obj = resultSet.getObject(index);
		}catch(SQLException sqlException) {
			return null;
		}
	    return obj.toString();
	}
	
	public int returnAmount(String id, String answer) {
		resultSet = executeQuery(statement, "SELECT " + answer + " FROM RESULT WHERE ID_SURVEY =" + id + ";");
		try {
			resultSet.next();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return Integer.parseInt(readValueOne());
	}

	public void insertIntoResult(String id, char answer1, char answer2, char answer3) {
		int current1AAmount = returnAmount(id, "ANSWER1A");
		int current1BAmount = returnAmount(id, "ANSWER1B");
		int current1CAmount = returnAmount(id, "ANSWER1C");

		int current2AAmount = returnAmount(id, "ANSWER2A");
		int current2BAmount = returnAmount(id, "ANSWER2B");
		int current2CAmount = returnAmount(id, "ANSWER2C");
		
		int current3AAmount = returnAmount(id, "ANSWER3A");
		int current3BAmount = returnAmount(id, "ANSWER3B");
		int current3CAmount = returnAmount(id, "ANSWER3C");
		
		if(answer1 == 'a') {
			current1AAmount++;
		}
		if(answer1 == 'b') {
			current1BAmount++;
		}
		if(answer1 == 'c') {
			current1CAmount++;
		}
		if(answer2 == 'a') {
			current2AAmount++;
		}
		if(answer2 == 'b') {
			current2BAmount++;
		}
		if(answer2 == 'c') {
			current2CAmount++;
		}
		if(answer3 == 'a') {
			current3AAmount++;
		}
		if(answer3 == 'b') {
			current3BAmount++;
		}
		if(answer3 == 'c') {
			current3CAmount++;
		}
		
		executeUpdate(statement, "UPDATE RESULT SET ANSWER1A = " + current1AAmount + ", ANSWER1B = " + current1BAmount + ", ANSWER1C = " 
		+ current1CAmount + ", ANSWER2A = " + current2AAmount + ", ANSWER2B = " +current2BAmount + ", ANSWER2C = " + current2CAmount 
		+ ", ANSWER3A = " + current3AAmount + ", ANSWER3B = " + current3BAmount + ", ANSWER3C = " + current3CAmount + " WHERE ID_SURVEY = " 
		+ id + ";");
	}
	
	public void insertIntoAnswer(String id, String idUser, char answer1, char answer2, char answer3) {
		executeUpdate(statement, "INSERT INTO ANSWER(ID_SURVEY, ID_USER, ANSWER1, ANSWER2, ANSWER3) VALUES(" + id + "," + idUser + ",'"+ answer1 + "','" + answer2 + "','" + answer3 + "')");
	}
	
	public void closeConnection() {
	    try {
	        statement.close();
	        connection.close();
	    } catch (SQLException e) {
	        System.out.println("Mistake when closing the connection " + e.toString());
	        System.exit(4);
	    }
	}
	
	public Statement getStatement() {
		return statement;
	}
	
}
