
import java.io.*; 
import java.net.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
 
public class Server{ 
	
    public static void main(String[] args) throws IOException { 
       
    	ServerSocket serverSocket = new ServerSocket(5056); 
        ExecutorService exe = Executors.newFixedThreadPool(100);
        DatabaseHandler database = new DatabaseHandler();

        while (true){ 
            Socket socket = null; 
              
            try { 
                socket = serverSocket.accept(); 
                  
                System.out.println("A new client is connected : " + socket); 
                
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
  
                ServerThreadTCP serverThreadTCP = new ServerThreadTCP(socket, bufferedReader, printWriter, database);
				exe.execute(serverThreadTCP);
                  
            } 
            catch (Exception e){ 
                socket.close(); 
                e.printStackTrace(); 
            } 
        } 
    } 
}
	class ServerThreadTCP implements Runnable{ 
		 String a;
		 String b;
		 final   BufferedReader in;
		 final   PrintWriter out;
		 final Socket s; 
		 final DatabaseHandler database;
		 
		 public ServerThreadTCP(Socket s, BufferedReader in, PrintWriter out, DatabaseHandler database) { 
		     this.s = s; 
		     this.in = in; 
		     this.out = out; 
		     this.database = database;
		 } 
		
		 @Override
		 public void run() { 
		     String numberSurvey; 
		     String numberQuestion;
		     String numberFirstAnswer;
		     String numberSecondAnswer;
		     String numberThirdAnswer;
		     String answer1;
		     String answer2;
		     String answer3;
		     String toreturn; 
		     if (true) { 
		         try { 
		        	 out.println(database.getSurveyAmount());
		        	 out.println(database.getNameSurveys());
		        	 out.flush();
		        	 
		             numberSurvey = in.readLine();
		             numberQuestion = in.readLine();
		             numberFirstAnswer = in.readLine();
		             numberSecondAnswer = in.readLine();
		             numberThirdAnswer = in.readLine();
		             
		             out.println(database.getQuestionSurvey(numberSurvey, numberQuestion, numberFirstAnswer, numberSecondAnswer, numberThirdAnswer));
		             out.flush();
		             
		             answer1 = in.readLine();
		             System.out.println(answer1);
		             numberQuestion = in.readLine();
		             numberFirstAnswer = in.readLine();
		             numberSecondAnswer = in.readLine();
		             numberThirdAnswer = in.readLine();
		             
		             out.println(database.getQuestionSurvey(numberSurvey, numberQuestion, numberFirstAnswer, numberSecondAnswer, numberThirdAnswer));
		             out.flush();
		             
		             answer2 = in.readLine();
		             System.out.println(answer2);
		             numberQuestion = in.readLine();
		             numberFirstAnswer = in.readLine();
		             numberSecondAnswer = in.readLine();
		             numberThirdAnswer = in.readLine();
		             
		             out.println(database.getQuestionSurvey(numberSurvey, numberQuestion, numberFirstAnswer, numberSecondAnswer, numberThirdAnswer));
		             out.flush();
		             
		             answer3 = in.readLine();
		             System.out.println(answer3);
		             
		             s.close();
		             
		             
		         } catch (IOException e) { 
		             e.printStackTrace(); 
		         } catch (SQLException e) {
					e.printStackTrace();
				} 
		
		     }
		 }
}

	 